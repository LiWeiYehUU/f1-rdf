import requests 
from bs4 import BeautifulSoup
import pandas as pd

url = "https://www.statsf1.com/en/statistiques/pilote/victoire/grille.aspx"

def import_data():
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36'
    headers = {'User-Agent': user_agent}
    page = requests.get(url,headers=headers)
    soup = BeautifulSoup(page.content, "html.parser")
    result = soup.find(id="main")
    table = result.find_all("div", class_="SimpleBar")
    return table

def main():
    table = import_data()
    print(table)

if __name__ == "__main__":
    main()

#references
#https://realpython.com/beautiful-soup-web-scraper-python/
#https://stackoverflow.com/questions/25491872/request-geturl-returns-empty-content
