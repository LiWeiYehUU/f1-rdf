from bs4 import BeautifulSoup
import requests

BASE_URL = "https://f1.fandom.com/wiki/"

# display scrapped data
# print(soup.prettify())

def main():
    scraper = DriverScraper()

    # Create while loop to loop through all drivers
    scraper.scrape_driver("Michael_Schumacher")


class DriverScraper():

    def scrape_driver(self, name):
        page = requests.get(BASE_URL + name, timeout=10)

        # scrape webpage
        soup = BeautifulSoup(page.content, 'html.parser')
        infobox = soup.find("aside", {"class":"portable-infobox"})
        infobox_sections = infobox.find_all("section")
        infobox_driver_information = infobox_sections[0]
        infobox_career_statistics = infobox_sections[1]

        birthdate = self.get_birthdate(infobox_driver_information)
        print(birthdate.prettify())

        # TODO: create a file that creates the rdf

    def get_birthdate(self, driver_information):
        # TODO: make this return date of birth
        return driver_information.find("div", {"data-source":"birth date"}, recursive=False)

if __name__ == "__main__":
    main()
