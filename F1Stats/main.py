from flask import Flask, render_template, request
import requests
import SPARQLWrapper

app = Flask(__name__)

user_agent = "FormulaOneStatsMKDE2022 (https://gitlab.com/LiWeiYehUU/f1-rdf)"

requests_session = requests.Session()
requests_session.headers.update({
    'Accept': 'application/json',
    'User-Agent': user_agent,
})


sparql_session = SPARQLWrapper.SPARQLWrapper("https://query.wikidata.org/sparql",
                                             agent=user_agent)
sparql_session.setReturnFormat(SPARQLWrapper.JSON)



def get_driver_ids():
    query = """
        SELECT DISTINCT ?driver ?driverLabel
        WHERE {
        ?driver wdt:P31 wd:Q5;
                wdt:P106 wd:Q10841764;
                wdt:P569 ?databirth;
                wdt:P19 ?placebirth;
                wdt:P21 ?gender;
                wdt:P1355 ?winTimes;
                wdt:P2522 ?vic;
                wdt:P18 ?img;
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        }
    """
    sparql_session.setQuery(query)
    result = sparql_session.query().convert()

    driver_item_ids = []
    driver_item_labels = []
    # gets the driver ids and labels from the query
    for binding in result['results']['bindings']:
        # add id
        driver_uri = binding['driver']['value']
        assert driver_uri .startswith("http://www.wikidata.org/entity/") # should work
        driver_item_id = driver_uri[len('http://www.wikidata.org/entity/'):]
        driver_item_ids.append(driver_item_id)

        # # add label
        driver_item_label = binding['driverLabel']['value']
        driver_item_labels.append(driver_item_label)

    labels = {}
    for index, id in enumerate(driver_item_ids):
        labels[id] = driver_item_labels[index]

    driver_item_ids.sort()

    return driver_item_ids, labels

def get_championship_ids():
    query = """
        SELECT ?championship ?championshipLabel WHERE{
            ?championship wdt:P31 wd:Q108861375;
                wdt:P1346 ?winName;
                wdt:P580 ?y.
            ?winName wdt:P1559 ?name
            SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        }
        ORDER BY DESC(?y)
    """
    sparql_session.setQuery(query)
    result = sparql_session.query().convert()

    championship_item_ids = []
    championship_item_labels = []

    # gets the driver ids and labels from the query
    for binding in result['results']['bindings']:
        # add id
        championship_uri = binding['championship']['value']
        assert championship_uri.startswith("http://www.wikidata.org/entity/") # should work
        championship_item_id = championship_uri[len('http://www.wikidata.org/entity/'):]
        championship_item_ids.append(championship_item_id)

        # # add label
        championship_item_label = binding['championshipLabel']['value']
        championship_item_labels.append(championship_item_label)

    labels = {}
    for index, id in enumerate(championship_item_ids):
        labels[id] = championship_item_labels[index]

    return championship_item_ids, labels

driver_ids, driver_labels = get_driver_ids()
championship_ids, championship_labels = get_championship_ids()

@app.route('/')
def index():
    return render_template('index.html', driver_idss=driver_ids, championship_idss=championship_ids)

@app.route('/driver', methods=['POST'])
def driver():
    driver_id = request.form['driver_id']
    driver_name = driver_labels[driver_id]
    query = """
        SELECT distinct ?person ?nationalityLabel ?personLabel ?databirth ?placebirthLabel ?img
                        ?genderLabel ?startTime ?endTime ?winTimes ?vic ?vicLabel
        WHERE
        {
        bind(wd:%s as ?person)  # change wd: to corresponding id
        ?person wdt:P27 ?nationality.
        optional {?person wdt:P569 ?databirth}.
        optional {?person wdt:P19 ?placebirth}.
        optional {?person wdt:P21 ?gender}.
        optional {?person wdt:P580 ?startTime}.
        optional {?person wdt:P582 ?endTime}.
        optional {?person wdt:P1355 ?winTimes}.
        optional {?person wdt:P2522 ?vic}.
        optional {?person wdt:P18 ?img}
        optional {?vic wdt:P3450 ?competition}.
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        }
    """ % driver_id

    sparql_session.setQuery(query)
    result = sparql_session.query().convert()

    driver_gender = result['results']['bindings'][0]['genderLabel']['value']
    driver_nationality = result['results']['bindings'][0]['nationalityLabel']['value']
    driver_dateOfBirth = result['results']['bindings'][0]['databirth']['value']
    driver_placeOfBirth = result['results']['bindings'][0]['placebirthLabel']['value']
    driver_wins = result['results']['bindings'][0]['winTimes']['value']
    driver_img = result['results']['bindings'][0]['img']['value']

    return render_template('index.html',
                          driver_idss=driver_ids,
                          championship_idss=championship_ids,
                          driver_id=driver_id,
                          driver_name=driver_name,
                          driver_nationality=driver_nationality,
                          driver_dateOfBirth=driver_dateOfBirth,
                          driver_placeOfBirth=driver_placeOfBirth,
                          driver_gender=driver_gender,
                          driver_wins=driver_wins,
                          driver_img=driver_img)

@app.route('/race', methods=['POST'])
def race():
    championship_id = request.form['championship_id']
    championship_name = championship_labels[championship_id]
    query = """
        SELECT distinct ?championshipLabel ?name ?y ?organizerLabel ?teamsLabel ?startTime ?endTime ?particiNum ?winnerLabel ?racesNum
            WHERE
            {
            bind(wd:%s as ?championship)  # change wd: to corresponding id
            ?championship wdt:P1346 ?winName;
                            wdt:P580 ?y.
            ?winName wdt:P1559 ?name
            optional {?championship wdt:P580 ?startTime}.
            optional {?championship wdt:P582 ?endTime}.
            optional {?championship wdt:P664 ?organizer}.
            optional {?championship wdt:P1132 ?particiNum}.
            optional {?championship wdt:P1923 ?teams}.
            optional {?championship wdt:P1346 ?winner}.
            optional {?championship wdt:P1350 ?racesNum}.
            SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        }
    """ % championship_id

    sparql_session.setQuery(query)
    result = sparql_session.query().convert()

    championship_winner = result['results']['bindings'][0]['name']['value']
    championship_date = result['results']['bindings'][0]['y']['value']
    championship_organizer = result['results']['bindings'][0]['organizerLabel']['value']
    championship_start = result['results']['bindings'][0]['startTime']['value']
    championship_end = result['results']['bindings'][0]['endTime']['value']
    championship_noOfRaces = result['results']['bindings'][0]['racesNum']['value']

    teams = []
    for binding in result['results']['bindings']:
        # check if key exists
        if ("teamsLabel") in binding:
            team = binding['teamsLabel']['value']
            if team not in teams:
                teams.append(team)

    return render_template('index.html',
                          driver_idss=driver_ids,
                          championship_idss=championship_ids,
                          no_teams_known=len(teams) == 0,
                          championship_id=championship_id,
                          championship_name=championship_name,
                          championship_winner=championship_winner,
                          championship_date=championship_date,
                          championship_organizer=championship_organizer,
                          championship_start=championship_start,
                          championship_end=championship_end,
                          championship_noOfRaces=championship_noOfRaces,
                          championship_teams=teams)

@app.route('/youngest_winner', methods=['GET'])
def youngest_winner():
    query = """
        SELECT ?championshipLabel ?winnerLabel ?victoryDate ?dateOfBirth ?age
        WHERE {
        ?championship wdt:P31 wd:Q108861375;
            wdt:P1346 ?winner ;
            wdt:P582 ?victoryDate .
        #?winner wdt:569 ?dob
        ?winner wdt:P569 ?dateOfBirth.
        bind(ROUND((?victoryDate - ?dateOfBirth) / 365.2564) as ?age)
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        }
        ORDER BY ASC(?age)
        LIMIT 1
    """
    sparql_session.setQuery(query)
    result = sparql_session.query().convert()

    driver_name = result['results']['bindings'][0]['winnerLabel']['value']
    driver_vic_date = result['results']['bindings'][0]['victoryDate']['value']
    driver_age = result['results']['bindings'][0]['age']['value']
    driver_championship_label = result['results']['bindings'][0]['championshipLabel']['value']

    # return result

    return render_template('index.html',
                            youngest_winner=True,
                            driver_idss=driver_ids,
                            championship_idss=championship_ids,
                            driver_name=driver_name,
                            driver_vic_date=driver_vic_date,
                            driver_age=driver_age,
                            driver_championship_label=driver_championship_label
)

@app.route('/oldest_winner', methods=['GET'])
def oldest_winner():
    query = """
        SELECT ?championshipLabel ?winnerLabel ?victoryDate ?dateOfBirth ?age
        WHERE {
        ?championship wdt:P31 wd:Q108861375;
            wdt:P1346 ?winner ;
            wdt:P582 ?victoryDate .
        ?winner wdt:P569 ?dateOfBirth.
        bind(ROUND((?victoryDate - ?dateOfBirth) / 365.2564) as ?age)
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        }
        ORDER BY DESC(?age)
        LIMIT 1
    """
    sparql_session.setQuery(query)
    result = sparql_session.query().convert()

    driver_name = result['results']['bindings'][0]['winnerLabel']['value']
    driver_vic_date = result['results']['bindings'][0]['victoryDate']['value']
    driver_age = result['results']['bindings'][0]['age']['value']
    driver_championship_label = result['results']['bindings'][0]['championshipLabel']['value']

    # return result

    return render_template('index.html',
                            oldest_winner=True,
                            driver_idss=driver_ids,
                            championship_idss=championship_ids,
                            driver_name=driver_name,
                            driver_vic_date=driver_vic_date,
                            driver_age=driver_age,
                            driver_championship_label=driver_championship_label
)

@app.route('/avg_age', methods=['GET'])
def avg_age():
    query = """
        SELECT (ROUND(AVG(?age)) AS ?avgAge)
        WHERE {
        ?championship wdt:P31 wd:Q108861375;
            wdt:P1346 ?winner ;
            wdt:P582 ?victoryDate .
        ?winner wdt:P569 ?dateOfBirth.
        bind(ROUND((?victoryDate - ?dateOfBirth) / 365.2564) AS ?age)
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        }
    """
    sparql_session.setQuery(query)
    result = sparql_session.query().convert()

    avg_age_drivers = result['results']['bindings'][0]['avgAge']['value']

    return render_template('index.html',
                            avg_age_tf=True,
                            driver_idss=driver_ids,
                            championship_idss=championship_ids,
                            avg_age_drivers=avg_age_drivers,
)

@app.route('/country_selected_as_hq', methods=['GET'])
def country_selected_as_hq():
    query = """
        SELECT ?countryLabel (COUNT(?country) as ?teams) WHERE{
        ?v wdt:P31 wd:Q10497835;
            wdt:P159 ?city.
        ?city wdt:P17 ?country.
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        }
        GROUP BY ?countryLabel
        ORDER BY DESC(?teams)
    """
    sparql_session.setQuery(query)
    result = sparql_session.query().convert()

    countries = []
    teams = []
    for binding in result['results']['bindings']:
        countries.append(binding['countryLabel']['value'])
        teams.append(binding['teams']['value'])

    country_team_zip = zip(countries, teams)

    return render_template('index.html',
                            country=True,
                            driver_idss=driver_ids,
                            championship_idss=championship_ids,
                            country_team_zip=country_team_zip,
)


@app.route('/out_top_10_won', methods=['GET'])
def out_top_10_won():
    df = import_data(url, True)

    query = """
    SELECT DISTINCT ?d WHERE{
        ?s ?p ?o;
           ?h ?link.
        ?s2 ?url ?l;
            ?driver ?d.
    FILTER(?p = "Starting"^^xsd:string && ?h = "href"^^xsd:string && ?o > "10"^^xsd:integer && ?url = "url"^^xsd:string && ?driver = "Driver"^^xsd:string && ?l = ?link)
    }
    """
    #Just filling the string literal in like "?s "Starting"^^xsd:string ?o. did not work and resulted in an error

    list = df["Starting grid"].tolist()
    list= list[:-1]
    list = [int(x) for x in list]
    df = get_url(df, list)
    df = df[['url','Starting grid','href','Driver']] #tried this to reduce the graph but it did not work which does not allow furhter quering but makes it "faster"
    g = rdfpandas.to_graph(df)

    qresult = g.query(query)
    for row in qresult:
        print(row[0])

    return qresult
    # should return render_template(), displaying the winners that started outside of the top 10

# Mervin stuff
# import requests
from bs4 import BeautifulSoup
import pandas as pd
import rdfpandas
import rdflib
from rdflib import Graph
import numpy as np

url = "https://www.statsf1.com/en/statistiques/pilote/victoire/grille.aspx"
baseurl = "https://www.statsf1.com/en/statistiques/pilote/victoire/"

def import_data(purl,grille):
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36'
    headers = {'User-Agent': user_agent}
    page = requests.get(purl,headers=headers)
    soup = BeautifulSoup(page.content, "html.parser")
    result = soup.find(id="main")
    table = result.find_all("div", class_="SimpleBar")
    links = []
    df = pd.read_html(str(table), flavor='bs4', header=[0])[0]
    if grille:
        for a_href in result.find_all("a", href=True):
            if "grille" in a_href["href"]:
                links = links + [baseurl + a_href["href"]]
        links = links + [np.NaN]
        df.insert(3, "href",links, True)
    df.insert(0,"url",purl,True)
    return df

def get_url(df, ns):
    for n in ns:
        gurl = "https://www.statsf1.com/en/statistiques/pilote/victoire/detail-{}--grille.aspx".format(n)
        new_df = import_data(gurl, False)
        df = pd.concat([df, new_df])
    return df

#references
#https://realpython.com/beautiful-soup-web-scraper-python/
#https://stackoverflow.com/questions/25491872/request-geturl-returns-empty-content
